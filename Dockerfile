# Use uma imagem base do Node.js
FROM node:14

# Diretório de trabalho dentro do contêiner
WORKDIR /app

# Copie o arquivo package.json e package-lock.json
COPY package*.json ./

# Instale as dependências
RUN npm install

# Copie o restante dos arquivos do projeto para o contêiner
COPY . .

# Exponha a porta que seu aplicativo usa
EXPOSE 3000

# Comando para iniciar o aplicativo
CMD ["npm", "start"]
